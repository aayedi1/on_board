
import "../node_modules/react-calendar/dist/Calendar.css";
import TodoList from "./modules/to-do/views/TodoList";

function App() {
    return (
        <div className="App">
            <div>
                <TodoList/>
            </div>
        </div>
    );
}

export default App;
