import {
    Divider,
    IconButton, List,
    ListItem,
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText, makeStyles,
    TextField
} from "@material-ui/core";
import {useState} from "react";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import RemoveCircle from "@material-ui/icons/RemoveCircle";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import {removeTodo, updateTodo} from "../services/TodoService";

export default function ToDoItem(props) {

    // to be refactored on store integration ...
    const removeItem = (id) => {
        removeTodo(id).then((payload) => {
            console.log(id);
            toggleDeleted(true);
            console.log("deleted");
        })
    }

    let item = props.item;
    const [description, changeTodo] = useState(props.item.title);
    const [isDeleted, toggleDeleted] = useState(false);
    const [isDone, toggleIsDone] = useState(props.item.isDone);
    const markAsDone = () => {
        item.isDone = true;
        updateTodo(item.id, item).then(r => {
            console.log("is done");
            toggleIsDone(true);
        })
    }
    return (
        <>
            {
                isDeleted ? (
                    <></>
                ) : (<ListItem>
                    <ListItemIcon>
                        <HourglassEmptyIcon color={"disabled"}/>
                    </ListItemIcon>

                    {isDone ? (
                            <>
                                <TextField value={description} disabled={true}/>
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" aria-label="delete" onClick={(e) => removeItem(item.id)}>
                                        <RemoveCircle color={"action"}/>
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </>

                        ) :
                        (
                            <>
                                <TextField value={description} onChange={(e) => {
                                    changeTodo(e.target.value)
                                    item.title = description;
                                    updateTodo(item.id,item).then((payload) =>{
                                        console.log('element updated');
                                        item = payload.data;
                                    })
                                }}
                                           disabled={false}/>
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" aria-label="delete" onClick={(e) => removeItem(item.id)}>
                                        <RemoveCircle color={"action"}/>
                                    </IconButton>
                                    <IconButton edge={"end"} onClick={(e) => markAsDone()}>
                                        <CheckCircleIcon/>
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </>
                        )}

                </ListItem>)
            }
        </>

    )
}
