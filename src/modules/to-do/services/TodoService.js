import axios from 'axios';
import {ENDPOINT} from "../../../environnement";

export function fetchTodos(){
    return axios.get(`${ENDPOINT}/to-do`);
}

export function removeTodo(id){
    return axios.delete(`${ENDPOINT}/to-do/remove/${id}`);
}

export function newTodo(todo){
    return axios.post(`${ENDPOINT}/to-do/create-todo`,todo);
}

export function updateTodo(id,todo){
    return axios.put(`${ENDPOINT}/to-do/update/${id}`,todo);
}
