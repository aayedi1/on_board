import "../styles/todo.css"
import ToDoItem from "../components/TodoItem";
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import AddCircleRounded from '@material-ui/icons/AddCircleRounded';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import FiberNewIcon from '@material-ui/icons/FiberNew';
import {
    Button, ButtonGroup, Container, Divider, Grid, IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText,
    makeStyles, Paper, TextField
} from "@material-ui/core";
import {useEffect, useState} from "react";
import {fetchTodos, newTodo} from "../services/TodoService";
import LoaderComponent from "../components/LoaderComponent";
import TodoModel from "../models/TodoModel";

const useStyles = makeStyles((theme) => ({
    root: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        padding: '0 30px',
    },
    text: {
        color: '#fafafa'
    }
}));
export default function TodoList() {
    const [todos, loadTodos] = useState([]);
    const [isLoaded, toggleIsLoaded] = useState(false);
    const [isAddPressed, toggleAddPressed] = useState(false);
    const [newTitle, onTitleChange] = useState("");
    let cp = [];
    const onAddPressed = () => {
        toggleAddPressed(!isAddPressed);

    }
    const onAll = () => {
        const items = JSON.parse(localStorage.getItem('todos'));
        loadTodos(items);
        console.table(items);
    }
    const onDone = () => {
        const items = JSON.parse(localStorage.getItem('todos'));
        let elements = items.filter((x) => !x.isDone );
        loadTodos(elements);
    }

    const onUndone = () => {
        const items = JSON.parse(localStorage.getItem('todos'));
        let elements = items.filter((x) => x.isDone);
        console.table(elements);
        loadTodos(elements);
    }
//    const

    useEffect(() => {
        fetchTodos().then((payload) => {
            console.log(payload.data);
            toggleIsLoaded(true);
            localStorage.setItem('todos', JSON.stringify(payload.data));
            const items = JSON.parse(localStorage.getItem('todos'));
            console.log(items);
            loadTodos(items);
        })
    }, [])
    const classes = useStyles();
    const onConfirmPressed = () => {
        let todo = new TodoModel();
        todo.title = newTitle;
        // to be refactored on correcting the authentication bug on the backend side...
        todo.user.id = "5fd01b42c6965a1719004a44"
        todo.isDone = false;
        newTodo(todo).then(r => {
            console.log(r.data);
            console.log("pushed successfully !");
            let currentItems = todos;
            currentItems.push(r.data);
            loadTodos(currentItems);
            onTitleChange("");
            toggleAddPressed(false);
            cp= todos;
        }).catch((err) => {
            console.log(err);
        })
    }
    return (
        <Container maxWidth="sm" style={{"marginTop": "15rem"}}>
            {isLoaded ? (
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <List component="nav" className={classes.root} aria-label="contacts">
                                {isAddPressed ? (
                                        <ListItem>
                                            <ListItemIcon>
                                                <FiberNewIcon color={"disabled"}/>
                                            </ListItemIcon>
                                            <TextField value={newTitle} onChange={(e) => onTitleChange(e.target.value)}/>
                                            <ListItemSecondaryAction>
                                                <Button color={"primary"} onClick={(e) => onConfirmPressed()}>Confirm</Button>
                                            </ListItemSecondaryAction>
                                        </ListItem>
                                    ) :
                                    (
                                        <></>
                                    )}
                                {todos.map((todo, i) => {
                                    console.log(i);
                                    return (
                                        <ToDoItem key={i} item={todo}/>
                                    )
                                })}
                                <Divider/>
                                <ListItem>
                                    <ListItemText inset primary="Want to create a new task ?"/>
                                    <ListItemSecondaryAction>
                                        <IconButton edge="end" aria-label="delete" onClick={(e) => onAddPressed()}>
                                            <AddCircleRounded/>
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            </List>
                        </Grid>
                        <Grid item xs={12}>
                            <ButtonGroup disableElevation variant="contained">
                                <Button color={"primary"} onClick={(e) => onAll()}>All</Button>
                                <Button color={"secondary"} onClick={(e) => onDone()}>Done</Button>
                                <Button color={"default"} onClick={(e) => onUndone()}>Undone</Button>
                            </ButtonGroup>
                        </Grid>
                    </Grid>
                ) :
                (
                    <LoaderComponent/>
                )}

        </Container>
    );
}
